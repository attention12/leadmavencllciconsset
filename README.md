# Icon Pack

## Install & Update
```shell
npm install -S https://gitlab.com/attention12/leadmavencllciconsset
```

## Add to an Angular project
```shell
"styles": [
  ...
  "node_modules/icon-pack/dist/style.css",
  ...
],
```

## How to update
1. Update icon pack at the [IconMoon App](https://icomoon.io/app)
2. Replace `./src/lib` with data from the downloaded archive
3. Run `npm run build` or `sh ./build`
4. Run `npm install -S https://gitlab.com/attention12/leadmavencllciconsset` in your project for update npm package.